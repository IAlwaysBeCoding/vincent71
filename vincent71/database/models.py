from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, Sequence, Integer, \
                        String, Text, Boolean, DateTime, create_engine

from sqlalchemy.orm import scoped_session, sessionmaker

from scrapy.utils.project import get_project_settings

SETTINGS = get_project_settings()
DATABASE = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(
        SETTINGS['MYSQL_USERNAME'], SETTINGS['MYSQL_PASSWORD'],
        SETTINGS['MYSQL_HOST'], SETTINGS['MYSQL_PORT'] , SETTINGS['MYSQL_DATABASE'])

Base = declarative_base()
engine = create_engine(DATABASE, convert_unicode=True, echo=False)


class DatabaseException(Exception):
    pass

class Category(Base):

    __tablename__   = "categories"
    Id              = Column(Integer, autoincrement=True, primary_key=True)
    Name            = Column(String(32))
    Url             = Column(String(2048))
    ToCheck         = Column(String(4), server_default='yes')

class Product(Base):

    __tablename__   = "products"

    Id              = Column(Integer, autoincrement=True, primary_key=True)
    Url             = Column(String(2048))
    Title           = Column(String(128))
    Vendor          = Column(String(64), primary_key=True)
    Category        = Column(String(64))
    CategoryUrl     = Column(String(1024))
    UrlImage        = Column(String(2048))
    DiscoveryDate   = Column(DateTime)
    LastUpdate      = Column(DateTime)
    ScrapeInProgess = Column(String(18))
    LastStatus      = Column(Integer)

class Stock(Base):

    __tablename__      = "stocks"

    Id                 = Column(Integer, autoincrement=True, primary_key=True)
    Vendor             = Column(String(32))
    ProductId          = Column(Integer, ForeignKey('products.Id'))
    ProductUrl         = Column(String(2048))
    Date               = Column(DateTime)
    AvailableQty       = Column(Integer, primary_key=True)
    TotalOrders        = Column(Integer, primary_key=True)
    Vendor             = Column(String(64), primary_key=True)
    TotalOrdersSince6m = Column(Integer, primary_key=True)


Base.metadata.create_all(engine)

DBSession = scoped_session(sessionmaker())
DBSession.configure(bind=engine, autoflush=False, expire_on_commit=False)




