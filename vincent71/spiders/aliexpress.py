
import json
import re
import time
from datetime import timedelta, datetime

import arrow
from furl import furl

from scrapy import Spider, Request, Selector
from scrapy.signals import engine_stopped

from vincent71.items import ProductItem, StockItem
from vincent71.utils import strip_spaces


from vincent71.database.models import DatabaseException, DBSession, Product, Stock, Category

class AliExpressSpider(Spider):

    name = 'aliexpress'
    allowed_domains = ['aliexpres.com']
    start_urls = ['https://aliexpress.com']

    def __init__(self, *args, **kwargs):

        mode = kwargs.get('mode', 'product')
        minimum_days_check = kwargs.get('minimum_days_check', 1)
        self.mode = self._parse_mode(mode)
        self.minimum_days_check = self._parse_minimum_days_check(minimum_days_check)


    def _parse_mode(self, mode):

        if not(mode == 'product' or mode == 'category'):
            raise ValueError('mode needs to be either product or category')

        return mode

    def _parse_minimum_days_check(self, minimum_days_check):

        try:
            return int(minimum_days_check)
        except:
            raise ValueError('minimum_days_check needs to be an int')

    def start_requests(self):

        if self.mode == 'product':

            for update_product in self.get_products_to_update():
                yield update_product

        elif self.mode == 'category':

            for update_category in self.get_categories_to_scrape():
                yield update_category

    def get_products_to_update(self):

        t = arrow.get(datetime.now()-timedelta(days=self.minimum_days_check))
        now = t.format('YYYY-MM-DD HH:mm:ss')
        get_ready = DBSession.query(Product) \
            .filter((Product.LastUpdate <= now) | (Product.LastUpdate == None))

        for p in get_ready.all():
            yield Request(
                p.Url,
                meta={
                    'Url' : p.Url,
                    'Title' : p.Title,
                    'UrlImage' : p.UrlImage,
                    'Category' : p.Category,
                    'CategoryUrl' : p.Category,
                    'Vendor' : p.Vendor,
                    'DiscoveryDate' : p.DiscoveryDate,
                    'LastUpdate' : p.LastUpdate,
                },
                callback=self.parse_product_detail)

            p.ScrapeInProgress = 'done'
            try:
                DBSession.commit()
            except Exception as e:
                DBSession.rollback()
                raise DatabaseException(e)


    def get_categories_to_scrape(self):

        get_ready = DBSession.query(Category) \
            .filter(Category.ToCheck == 'yes')

        for c in get_ready.all():
            yield Request(
                c.Url,
                meta={
                    'Url' : c.Url,
                },
                callback=self.parse_category)

            c.ToCheck = 'no'

            try:
                DBSession.commit()
            except Exception as e:
                DBSession.rollback()
                raise DatabaseException(e)


    def parse_product_detail(self, response):

        yield self.parse_product(response)
        yield self.parse_stock(response)

    def parse_product(self, response):

        now = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

        product_item = ProductItem()
        product_item['Url'] = response.meta['Url']
        product_item['Title'] = response.meta['Title']
        product_item['UrlImage'] = response.meta['UrlImage']
        product_item['Category'] = response.meta['Category']
        product_item['Vendor'] = response.meta['Vendor']
        product_item['DiscoveryDate'] = response.meta['DiscoveryDate']
        product_item['LastUpdate'] = now
        product_item['LastStatus'] = response.status

        return product_item

    def parse_stock(self, response):

        now = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

        stock_item = StockItem()
        stock_item['Vendor'] = response.meta['Vendor']
        stock_item['ProductId'] = response.xpath('//input[@name="objectId"]/@value').extract_first()
        stock_item['ProductUrl'] = response.url
        stock_item['Date'] = now
        stock_item['AvailableQty'] = self.get_available_qty(response)
        total_orders = response.xpath('//span[@id="j-order-num"]/text()').extract_first()
        stock_item['TotalOrders'] = total_orders.split(' ')[0] if total_orders else ''

        f = furl('https://feedback.aliexpress.com/display/evaluationProductDetailAjaxService.htm')
        f.args['productId'] = response.meta['ProductId']
        f.args['type'] = 'default'
        f.args['page'] = 1

        response.meta['stock_item'] = stock_item

        yield Request(str(f.url),
                      meta=response.meta,
                      callback=self.get_total_orders_6_months)

    def get_available_qty(self, response):
        find_qty = re.search(b"(?<=availQuantityForCustomer: ')(.*?)(?=')", response.body, re.DOTALL)

        if find_qty:
            return str(find_qty.group(0), 'utf-8')

    def get_total_orders_6_months(self, response):

        data = json.loads(response.body)
        stock_item = response.meta['stock_item']
        stock_item['TotalOrdersSince6m'] = data['range']['transactions']

        yield stock_item

    def parse_category(self, response):

        browse_products = response.xpath('//li[contains(@class, "list-item")]').extract()

        for browse_product in browse_products:

            now = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

            brsel = Selector(text=browse_product)
            product_item = ProductItem()
            product_item['Url'] = brsel.xpath('//a[contains(@class,"product")]/@href').extract_first()
            product_item['Title'] = brsel.xpath('//a[contains(@class,"product")]/@title').extract_first()
            product_item['UrlImage'] = brsel.xpath('//a[contains(@class,"picRind")]/@href').extract_first()
            product_item['CategoryUrl'] = response.url
            product_item['Category'] = response.xpath('//span[@class="current-cate"]/text()').extract_first()
            product_item['Vendor'] = 'AliExpress'
            product_item['DiscoveryDate'] = None
            product_item['LastUpdate'] = None

            self.clean_up_product_item(product_item, response)

            yield product_item


        has_next_page = response.xpath('//a[@class="page-next ui-pagination-next"]/@href').extract_first()

        if has_next_page:
            yield Request(
                response.urljoin(has_next_page),
                meta=response.meta,
                callback=self.parse_category,
                dont_filter=True
            )

    def clean_up_product_item(self, item, response):

        if item['UrlImage']:
            item['UrlImage'] = response.urljoin(item['UrlImage'])

        if item['Url']:
            item['Url'] = response.urljoin(item['Url'])

        if item['Category']:
            item['Category'] = response.urljoin(item['Category'])


