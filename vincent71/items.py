# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html


from scrapy import Item, Field

class ProductItem(Item):

    Id = Field()
    Url = Field()
    Title = Field()
    Vendor = Field()
    Category = Field()
    CategoryUrl = Field()
    UrlImage = Field()
    DiscoveryDate = Field()
    LastUpdate = Field()
    ScrapeInProgess = Field()
    LastStatus = Field()

class StockItem(Item):

    Id = Field()
    Vendor = Field()
    ProductId = Field()
    ProductUrl = Field()
    Date = Field()
    AvailableQty = Field()
    TotalOrders = Field()
    TotalOrdersSince6m = Field()

