
from datetime import timedelta, datetime
import time

from scrapy.utils.project import get_project_settings
from scrapy.signals import engine_stopped

from vincent71.spiders.aliexpress import AliExpressSpider
from vincent71.items import StockItem, ProductItem
from vincent71.database.models import DBSession, Category, Stock, Product


class DatabaseExporter(object):

    def process_item(self, item, spider):

        if isinstance(item, ProductItem):
            self.insert_product(item)

        elif isinstance(item, StockItem):
            self.insert_stock(item)


        return item

    @classmethod
    def from_crawler(cls, crawler):

        crawler.signals.connect(cls.close_db_session, signal=engine_stopped)

        return cls()

    @staticmethod
    def close_db_session():
        DBSession.close()

    def insert_product(self, item):

        discovery_date = self.get_discovery_date(item)

        if item['DiscoveryDate'] is None and item['LastUpdate'] is None:
            item['DiscoveryDate'] = discovery_date
        else:
            item['DiscoveryDate'] = discovery_date
            item['LastUpdate'] = discovery_date

        has_product = DBSession.query(Product) \
            .filter_by(Url=item['Url'])

        if has_product.first():
            product_item = has_product
            product_item.update(item)
        else:
            product_item = Product(**item)
            DBSession.add(product_item)

        try:
            DBSession.commit()
        except Exception as e:
            DBSession.rollback()
            raise DatabaseException(e)

    def insert_stock(self, item):

        has_stock = DBSession.query(Stock) \
            .filter(Stock.ProductUrl==item['ProductUrl'])

        if has_stock.first():
            stock_item = has_stock
            stock_item.update(item)
        else:
            stock_item = Stock(**item)
            DBSession.add(stock_item)

        try:
            DBSession.commit()
        except Exception as e:
            DBSession.rollback()
            raise DatabaseException(e)

    def get_discovery_date(self, item):

        has_discovery_date = DBSession.query(Product) \
            .filter(Product.Url == item['Url']) \
            .filter(Product.DiscoveryDate != None)

        if has_discovery_date.first():
            return has_discovery_date.first().DiscoveryDate
        else:
            now = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
            return now
